package com.example.yuanhao.mvvm.base

import android.app.Application
import android.content.Context

interface AppDelegate {

    fun attachBaseContext(base: Context?)

    fun onCreate(application: Application)

    fun onTerminate(application: Application)

}