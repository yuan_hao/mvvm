package com.example.yuanhao.mvvm.base

import android.os.Bundle
import android.view.View

interface IActivity {

    fun initView(savedInstanceState: Bundle?, setLayout: LayoutId)

    fun initData(savedInstanceState: Bundle?)

}

typealias LayoutId = (Int) -> Unit