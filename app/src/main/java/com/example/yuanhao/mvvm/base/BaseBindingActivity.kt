package com.example.yuanhao.mvvm.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.view.View
import android.view.ViewGroup
import com.example.yuanhao.mvvm.R

abstract class BaseBindingActivity<DB : ViewDataBinding> : BaseActivity() {

    lateinit var mBinding: DB

    override fun bindView(layoutId: Int) {
        mBinding = DataBindingUtil.setContentView(this@BaseBindingActivity, layoutId)
    }

    override fun createHeaderDecor(parent: ViewGroup): View? {

        val hader = layoutInflater.inflate(R.layout.layout_hader, parent, false)

        return hader
    }

}