package com.example.yuanhao.mvvm.base

import android.app.Application
import android.content.Context

class BaseApplication : Application() {

    private val mAppDelegate by lazy { AppDelegateImpl() }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        mAppDelegate.attachBaseContext(base)
    }

    override fun onCreate() {
        super.onCreate()
        mAppDelegate.onCreate(this)
    }


    override fun onTerminate() {
        super.onTerminate()
        mAppDelegate.onTerminate(this)
    }

}