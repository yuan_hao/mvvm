package com.example.yuanhao.mvvm.integration

interface IRepositoryManager {

    fun <R> obtainRemoteRepository(clazz: Class<R>): R

    fun <R> obtainLocalRepository(clazz: Class<R>): R

}