package com.example.yuanhao.mvvm.base

import android.os.Bundle
import android.support.annotation.LayoutRes

interface IFragment {

    @LayoutRes
    fun layoutId(): Int

    fun initData(savedInstanceState: Bundle?)
}