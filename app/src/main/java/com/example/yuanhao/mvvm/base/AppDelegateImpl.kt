package com.example.yuanhao.mvvm.base

import android.app.Application
import android.content.Context

class AppDelegateImpl : AppDelegate {

    override fun attachBaseContext(base: Context?) {
    }

    override fun onCreate(application: Application) {
    }

    override fun onTerminate(application: Application) {
    }
}