package com.example.yuanhao.mvvm.demo

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.example.yuanhao.mvvm.R
import com.example.yuanhao.mvvm.base.BaseActivity
import com.example.yuanhao.mvvm.base.LayoutId
import com.example.yuanhao.mvvm.demo.viewmodels.MainViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {


    override fun initView(savedInstanceState: Bundle?, setLayout: LayoutId) {
        setLayout(R.layout.activity_main)
//        mainViewModel(1)
        GlobalScope.launch {
            delay(1000L)
            println("world!")
        }
        println("hello,")
        Thread.sleep(2000L)

        val map = HashMap<String, String>()
        a(map)
    }

    override fun initData(savedInstanceState: Bundle?) {
//        mainViewModel.login()
    }

    override fun createHeaderDecor(parent: ViewGroup): View? {
        return layoutInflater.inflate(R.layout.layout_hader, parent, false)
    }


    fun a(map: Map<*, *>) {

    }
}