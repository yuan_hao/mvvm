package com.example.yuanhao.mvvm.base

import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout

open class HeaderHelper(var factory: Factory) {

    /* 头部视图  */
    var mHeaderDecor: View? = null
    //头部高度
    var mHeaderDecorHeight: Int = 0
    //是否显示了头部
    var mShowHeaderDecor = false

    fun addHeaderDecor(parent: ViewGroup) {
        mHeaderDecor = factory.createHeader(parent)
        when (parent) {
            is LinearLayout -> {
                mHeaderDecor?.apply {
                    mShowHeaderDecor = true
                    mHeaderDecorHeight = layoutParams.height
                    parent.addView(this, 0)
                }
            }
            is FrameLayout -> {
                val parentParams: FrameLayout.LayoutParams = parent.layoutParams as FrameLayout.LayoutParams
                parentParams.topMargin = mHeaderDecorHeight
            }
        }
    }

    interface Factory {
        /**
         * create header view
         * @param parent root
         * @return view
         */
        fun createHeader(parent: ViewGroup): View?
    }
}