package com.example.yuanhao.mvvm.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.security.InvalidParameterException

abstract class BaseBindingFragment<DB : ViewDataBinding> : Fragment(), IFragment {

    protected val TAG = javaClass.simpleName

    lateinit var mBinding: DB

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val id = layoutId()
        if (id != 0) {
            mBinding = DataBindingUtil.inflate(inflater, id, container, false)
            return mBinding.root
        } else {
            throw InvalidParameterException("The layout ID can not be 0")
        }
    }
}