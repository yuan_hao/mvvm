package com.example.yuanhao.mvvm.demo

import android.os.Bundle
import android.support.v4.app.Fragment
import com.example.yuanhao.mvvm.R
import com.example.yuanhao.mvvm.base.BaseFragment

/**
 * A simple [Fragment] subclass.
 *
 */
class TestFragment : BaseFragment() {


    override fun layoutId(): Int = R.layout.fragment_test

    override fun initData(savedInstanceState: Bundle?) {


    }

    companion object {
        @JvmStatic
        fun newInstance() =
                TestFragment().apply {
                    arguments = Bundle()
                }
    }
}
