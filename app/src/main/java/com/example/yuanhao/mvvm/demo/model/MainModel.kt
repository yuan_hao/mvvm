package com.example.yuanhao.mvvm.demo.model

import android.util.Log
import com.example.yuanhao.mvvm.base.BaseModel
import com.example.yuanhao.mvvm.integration.IRepositoryManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class MainModel : BaseModel() {

    lateinit var repositoryManager: IRepositoryManager

    fun login() {
        Log.d("tag", "登陆")
//        GlobalScope.async()
    }


    fun getUser() {
        Log.d("tag", "获取用户信息")

    }
}